package com.desenvlaet.managementfinance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ManagementfinanceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ManagementfinanceApplication.class, args);
    }

}
